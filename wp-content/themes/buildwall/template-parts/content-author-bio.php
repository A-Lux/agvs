<?php
/**
 * The template for displaying author bio.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Buildwall
 */

if ( ! get_the_author_meta( 'description' ) ) {
	return;
}
?>
<div class="post-author-bio">

	<div class="post-author__holder clear">
		<div class="post-author__avatar"><?php
			echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'buildwall_author_bio_avatar_size', 100 ), '', esc_attr( get_the_author_meta( 'nickname' ) ) );
		?></div>
		<div class="post-author__content">
			<h5 class="post-author__super-title"><?php printf(esc_html__( 'Writen by %s', 'buildwall' ), buildwall_get_the_author_posts_link() ); ?></h5>
			<p><?php
				echo get_the_author_meta( 'description' );
			?></p>
		</div>
	</div>
</div><!--.post-author-bio-->
