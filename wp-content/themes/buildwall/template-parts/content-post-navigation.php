<?php
/**
 * Template part for single post navigation.
 *
 * @package Buildwall
 */

if ( ! get_theme_mod( 'single_post_navigation', buildwall_theme()->customizer->get_default( 'single_post_navigation' ) ) ) {
	return;
}


$next_post_thumb = get_next_post();
$previous_post_thumb = get_previous_post();

if( ! empty( get_next_post() ) ){
	$next_post_thumb = get_the_post_thumbnail( $next_post_thumb->ID,'buildwall-thumb-100-76' ) ;
}
if( ! empty( get_previous_post() ) ){
	$previous_post_thumb = get_the_post_thumbnail( $previous_post_thumb->ID,'buildwall-thumb-100-76' );
}

the_post_navigation( array(
	'next_text' => '<div class="post-navigation__description-container"><h6 class="post-title">%title</h6><span class="meta-nav" aria-hidden="true">' . esc_html__( 'Next post', 'buildwall' ) . '</span> ' .
		'<span class="screen-reader-text">' . esc_html__( 'Next post:', 'buildwall' ) . '</span></div>' . $next_post_thumb,
	'prev_text' =>  $previous_post_thumb . '<div class="post-navigation__description-container"><h6 class="post-title">%title</h6><span class="meta-nav" aria-hidden="true">' . esc_html__( 'Prev post', 'buildwall' ) . '</span> ' .
		'<span class="screen-reader-text">' . esc_html__( 'Previous post:', 'buildwall' ) . '</span></div>',
) );