<?php
/**
 * Template part for displaying posts.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Buildwall
 */
?>

<?php
	$blog_layout_type    = get_theme_mod( 'blog_layout_type', buildwall_theme()->customizer->get_default( 'blog_layout_type' ) );
	$blog_featured_image = get_theme_mod( 'blog_featured_image', buildwall_theme()->customizer->get_default( 'blog_featured_image' ) );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item card' ); ?>>

	<div class="posts-list__right-col">

		<figure class="post-thumbnail"><?php
			buildwall_get_template_part( 'template-parts/post/post-components/post-image' );
			buildwall_get_template_part( 'template-parts/post/post-components/post-sticky' );
			buildwall_get_template_part( 'template-parts/post/post-meta/content-meta-categories' );
		?></figure><!-- .post-thumbnail -->

		<div class="posts-list__item-content">

			<header class="entry-header entry-meta"><?php
				buildwall_get_template_part( 'template-parts/post/post-components/post-title' );
				buildwall_get_template_part( 'template-parts/post/post-meta/content-meta-date' );
				buildwall_get_template_part( 'template-parts/post/post-meta/content-meta-author' );
				buildwall_get_template_part( 'template-parts/post/post-meta/content-meta-comments' );
				buildwall_get_template_part( 'template-parts/post/post-meta/content-meta-tags' );
			?></header><!-- .entry-header -->

			<div class="entry-content"><?php
				buildwall_get_template_part( 'template-parts/post/post-components/post-content' );
			?></div><!-- .entry-content -->

			<footer class="entry-footer">
				<div class="entry-footer-bottom entry-meta"><?php
					buildwall_get_template_part( 'template-parts/post/post-components/post-button' );
				?></div><!-- .entry-footer-bottom -->
			</footer><!-- .entry-footer -->
		</div><!-- .posts-list__item-content -->
	</div><!-- .posts-list__right-col -->

</article><!-- #post-## -->
