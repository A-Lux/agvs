<?php
/**
 * Template part for displaying sticky.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Buildwall
 */

$utility          = buildwall_utility()->utility;
$sticky           = buildwall_sticky_label( false );

echo wp_kses_post($sticky);
