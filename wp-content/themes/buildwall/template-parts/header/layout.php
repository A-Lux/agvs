<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Buildwall
 */

$header_contact_block_visibility = get_theme_mod( 'header_contact_block_visibility', buildwall_theme()->customizer->get_default( 'header_contact_block_visibility' ) );
$header_btn_visibility           = get_theme_mod( 'header_btn_visibility', buildwall_theme()->customizer->get_default( 'header_btn_visibility' ) );
$search_visible                  = get_theme_mod( 'header_search', buildwall_theme()->customizer->get_default( 'header_search' ) );
$header_woo_elements             = get_theme_mod( 'header_woo_elements', buildwall_theme()->customizer->get_default( 'header_woo_elements' ) );
?>
<div class="header-container_wrap container">

	<!--<?php if ( $header_contact_block_visibility || $header_btn_visibility ) : ?>
		<div class="header-row__flex header-components__contact-button header-components__grid-elements"><?php
			buildwall_contact_block( 'header' );
			buildwall_header_btn();
		?></div>
	<?php endif; ?> -->

	<div class="header-container__flex-wrap">
		<div class="header-container__flex">
			<div class="site-branding">
				<?php buildwall_header_logo() ?>
				<?php buildwall_site_description(); ?>
			</div>

			<div class="header-nav-wrapper">
				<?php buildwall_main_menu(); ?>
				<?php if ( $header_contact_block_visibility || $header_btn_visibility ) : ?>
					<div class="header-row__flex header-components__contact-button header-components__grid-elements"><?php
						buildwall_contact_block( 'header' );
						buildwall_header_btn();
					?></div>
				<?php endif; ?>
				<?php if ( $search_visible || $header_woo_elements ) : ?>
					<div class="header-components header-components__search-cart"><?php
						buildwall_header_search_toggle();
						buildwall_header_woo_cart();
					?></div>
				<?php endif; ?>

			</div>
		</div>

		<?php buildwall_header_search( '<div class="header-search">%s<span class="search-form__close"></span></div>' ); ?>
	</div>
</div>
