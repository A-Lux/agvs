<?php
/**
 * Template part for top panel in header.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Buildwall
 */

// Don't show top panel if all elements are disabled.
if ( ! buildwall_is_top_panel_visible() ) {
	return;
}
?>

<div <?php echo buildwall_get_html_attr_class( array( 'top-panel' ), 'top_panel_bg' ); ?>>
	<div class="container">
		<div class="top-panel__container">
			<?php buildwall_top_message( '<div class="top-panel__message">%s</div>' ); ?>
			<?php buildwall_contact_block( 'header_top_panel' ); ?>

			<div class="top-panel__wrap-items">
				<div class="top-panel__menus">
					<?php buildwall_top_menu(); ?>
					<?php buildwall_header_woo_currency_switcher(); ?>
					<?php buildwall_social_list( 'header' ); ?>
				</div>
			</div>
		</div>
	</div>
</div><!-- .top-panel -->
