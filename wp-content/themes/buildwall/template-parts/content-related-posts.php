<?php
/**
 * The template for displaying related posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Buildwall
 * @subpackage single-post
 */
?>
<div class="<?php echo esc_attr( $grid_class ); ?>">
	<article class="related-post hentry page-content">
		<figure class="post-thumbnail"><?php
			echo wp_kses_post( $category );
			echo wp_kses_post( $image );
		?></figure>
		<div class="related-post__content">
			<header class="entry-header">
				<?php
				echo wp_kses_post( $title );
				?>
				<div class="entry-meta"><?php
					echo wp_kses_post( $date );
					echo wp_kses_post( $author );
					echo wp_kses_post( $comment_count );
				?></div>
			</header>
			<div class="entry-content"><?php
				echo wp_kses_post( $excerpt );
			?></div>
			<footer class="entry-footer">
				<div class="entry-meta"><?php
					echo wp_kses_post( $tag );
				?></div>
			</footer>
		</div>
	</article><!--.related-post-->
</div>
