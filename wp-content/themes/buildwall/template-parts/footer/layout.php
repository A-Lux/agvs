<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package Buildwall
 */

$footer_logo_visibility    = get_theme_mod( 'footer_logo_visibility', buildwall_theme()->customizer->get_default( 'footer_logo_visibility' ) );
$footer_menu_visibility    = get_theme_mod( 'footer_menu_visibility', buildwall_theme()->customizer->get_default( 'footer_menu_visibility' ) );
?>

<div <?php buildwall_footer_container_class(); ?>>


	<div class="site-info container site-info-first-row">
		<div class="site-info-wrap">
			<div class="site-info-block"><?php
				buildwall_footer_logo();
			?></div>
			<div class="site-info-block__center">
			<?php buildwall_footer_menu();
				  buildwall_footer_copyright();?>
				  </div>
			<div class="site-info-block"><?php
				buildwall_social_list( 'footer' );
			?></div>
		</div>
		<?php buildwall_contact_block( 'footer' ); ?>
	</div>

</div><!-- .container -->
