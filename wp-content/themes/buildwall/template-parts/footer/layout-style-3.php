<?php
/**
 * The template for displaying the style-3 footer layout.
 *
 * @package Buildwall
 */
?>

<div <?php buildwall_footer_container_class(); ?>>


	<div class="site-info container site-info-first-row">
		<div class="site-info-wrap">
			<div class="site-info-block">
			<?php
				  buildwall_footer_logo();
				  buildwall_footer_menu();
				  buildwall_footer_copyright();?>
				  </div>
			<div class="site-info-block"><?php
				buildwall_social_list( 'footer' );
			?></div>
		</div>
		<?php buildwall_contact_block( 'footer' ); ?>
	</div>

</div><!-- .container -->
