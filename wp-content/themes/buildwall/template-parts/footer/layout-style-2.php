<?php
/**
 * The template for displaying the style-2 footer layout.
 *
 * @package Buildwall
 */
?>

<div <?php buildwall_footer_container_class(); ?>>
	<div class="site-info container"><?php
		buildwall_footer_logo();
		buildwall_footer_menu();
		buildwall_footer_copyright();
		buildwall_social_list( 'footer' );
	?></div><!-- .site-info -->
</div><!-- .container -->
