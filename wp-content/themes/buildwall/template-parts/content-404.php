<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Buildwall
 */

$btn_style_preset   = get_theme_mod( 'page_404_btn_style_preset', buildwall_theme()->customizer->get_default( 'page_404_btn_style_preset' ) );
$text_color         = get_theme_mod( 'page_404_text_color', buildwall_theme()->customizer->get_default( 'page_404_text_color' ) );
$additional_class   = ( 'light' === $text_color ) ? 'invert' : 'regular';

?>
<section class="error-404 not-found <?php esc_html( $additional_class ); ?>">
	<header class="page-header">
		<h1 class="page-title title-decoration__top title-decoration"><?php esc_html_e( '404', 'buildwall' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<h2><?php esc_html_e( 'Page Not Found', 'buildwall' ); ?></h2>
				<h6><?php esc_html_e( 'You may have typed the address incorrectly or you may have used an outdated link. Try search our site.', 'buildwall' ); ?></h6>
				<div class="search-wrap"><?php get_search_form(); ?></div>
				<p><a class="btn btn-<?php echo sanitize_html_class( $btn_style_preset ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Go home!', 'buildwall' ); ?></a></p>
			</div>
		</div>
	</div><!-- .page-content -->
</section><!-- .error-404 -->
