<?php
/**
 * Template part for posts pagination.
 *
 * @package Buildwall
 */

the_posts_pagination(
	array(
		'prev_text' => sprintf( '%s %s', '<i class="nc-icon-mini arrows-1_bold-left"></i>', esc_html__( 'PREV', 'buildwall' ) ),
		'next_text' => sprintf( '%s %s', esc_html__( 'NEXT', 'buildwall' ), '<i class="nc-icon-mini arrows-1_bold-right"></i>' ),
	)
);
