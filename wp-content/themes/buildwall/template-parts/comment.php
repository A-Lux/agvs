<?php
/**
 * The template for displaying comments.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy
 *
 * @package Buildwall
 */
?>
<div class="comment-author vcard">
	<?php echo buildwall_comment_author_avatar(); ?>
</div>
<div class="comment-content-wrap">
	<div class="comment-content-wrap__head">
		<footer class="comment-meta">
			<div class="comment-metadata">
				<?php echo buildwall_get_comment_author_link(); ?>
				<?php echo buildwall_get_comment_date(); ?>
			</div>
		</footer>
		<div class="comment-content">
			<?php echo buildwall_get_comment_text(); ?>
		</div>
	</div>
	<div class="reply">
		<?php echo buildwall_get_comment_reply_link( array(
			'reply_text' => esc_html__( 'Reply', 'buildwall' ),
		) ); ?>
	</div>
</div>
