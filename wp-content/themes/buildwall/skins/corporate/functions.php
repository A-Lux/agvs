<?php
/**
 * Corporate skin functions, hooks and definitions.
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Buildwall
 */

remove_filter( 'cherry_services_listing_templates_list', 'buildwall_cherry_services_listing_templates_list' );
add_filter( 'cherry_services_listing_templates_list', 'corporate_cherry_services_listing_templates_list' );

/**
 *  Add template to cherry services-list templates list;
 */
function corporate_cherry_services_listing_templates_list( $tmpl_list ) {

	$tmpl_list['default-icon']            = 'default-icon.tmpl';
	$tmpl_list['default-description']     = 'default-description.tmpl';
	$tmpl_list['media-icon']              = 'media-icon.tmpl';
	$tmpl_list['media-icon-background']   = 'media-icon-bg.tmpl';
	$tmpl_list['media-icon-background-2'] = 'media-icon-bg-2.tmpl';
	$tmpl_list['media-icon-background-3'] = 'media-icon-bg-3.tmpl';
	$tmpl_list['sidebar-media-icon']      = 'sidebar-media-icon.tmpl';

	return $tmpl_list;
}