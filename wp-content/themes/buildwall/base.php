<?php
/**
 * The base template.
 *
 * @package Buildwall
 */
?>
<?php get_header( buildwall_template_base() ); ?>

	<?php buildwall_site_breadcrumbs(); ?>

	<?php do_action( 'buildwall_render_widget_area', 'full-width-header-area' ); ?>

	<div <?php buildwall_content_wrap_class(); ?>>

		<?php do_action( 'buildwall_render_widget_area', 'before-content-area' ); ?>

		<div class="row">

			<div id="primary" <?php buildwall_primary_content_class(); ?>>

				<?php do_action( 'buildwall_render_widget_area', 'before-loop-area' ); ?>

				<main id="main" class="site-main" role="main">

					<?php include buildwall_template_path(); ?>

				</main><!-- #main -->

				<?php do_action( 'buildwall_render_widget_area', 'after-loop-area' ); ?>

			</div><!-- #primary -->

			<?php buildwall_get_sidebar(); ?>

		</div><!-- .row -->

		<?php do_action( 'buildwall_render_widget_area', 'after-content-area' ); ?>

	</div><!-- .site-content_wrap -->

	<?php do_action( 'buildwall_render_widget_area', 'after-content-full-width-area' ); ?>

<?php get_footer( buildwall_template_base() ); ?>
