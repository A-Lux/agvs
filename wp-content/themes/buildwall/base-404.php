<?php
/**
 * The base template for displaying 404 pages (not found).
 *
 * @package Buildwall
 */
$page_404_image     = get_theme_mod( 'page_404_image', buildwall_theme()->customizer->get_default( 'page_404_image' ) );
if ( $page_404_image ) {
	$page_404_image_url = esc_url( buildwall_render_theme_url( $page_404_image ) );
	$page_404_image_url = 'style="background-image:url(' . esc_url( $page_404_image_url ) . ')"';
}
?>
<?php get_header( buildwall_template_base() ); ?>

	<?php buildwall_site_breadcrumbs(); ?>
	<div class="bg-wrap-404" <?php echo $page_404_image_url ?>>
		<div <?php buildwall_content_wrap_class(); ?>>

			<div class="row">

				<div id="primary" <?php buildwall_primary_content_class(); ?>>

					<main id="main" class="site-main" role="main">

						<?php include buildwall_template_path(); ?>

					</main><!-- #main -->

				</div><!-- #primary -->

				<?php get_sidebar(); // Loads the sidebar.php. ?>

			</div><!-- .row -->

		</div><!-- .site-content_wrap -->
	</div>

<?php get_footer( buildwall_template_base() ); ?>
