<?php
/**
 * The template for displaying single tm-pg-album.
 *
 * @package Buildwall
 */
?>
<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<h3 class="entry-title title-decoration__bottom title-decoration"><?php the_title(); ?></h3>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php the_content(); ?>
			<?php do_action( 'tm-pg-grid-album', get_the_ID() ); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-<?php the_ID(); ?> -->
<?php endwhile; ?>
