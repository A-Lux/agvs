<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Buildwall
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php buildwall_get_page_preloader(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'buildwall' ); ?></a>
	<header id="masthead" <?php buildwall_header_class(); ?> role="banner">
		<?php buildwall_ads_header() ?>
		<?php buildwall_get_template_part( 'template-parts/header/mobile-panel' ); ?>
		<?php buildwall_get_template_part( 'template-parts/header/top-panel', buildwall_get_mod( 'header_layout_type', true, 'esc_attr' ) ); ?>

		<div <?php buildwall_header_container_class(); ?>>
			<?php buildwall_get_template_part( 'template-parts/header/layout', buildwall_get_mod( 'header_layout_type', true, 'esc_attr' ) ); ?>
		</div><!-- .header-container -->
	</header><!-- #masthead -->

	<div id="content" <?php buildwall_content_class(); ?>>
