<?php
/**
 * Menus configuration.
 *
 * @package Buildwall
 */

add_action( 'after_setup_theme', 'buildwall_register_menus', 5 );
/**
 * Register menus.
 */
function buildwall_register_menus() {

	register_nav_menus( array(
		'top'          => esc_html__( 'Top', 'buildwall' ),
		'main'         => esc_html__( 'Main', 'buildwall' ),
		'main_landing' => esc_html__( 'Landing Main', 'buildwall' ),
		'footer'       => esc_html__( 'Footer', 'buildwall' ),
		'social'       => esc_html__( 'Social', 'buildwall' ),
	) );
}
