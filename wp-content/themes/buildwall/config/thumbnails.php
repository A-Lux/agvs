<?php
/**
 * Thumbnails configuration.
 *
 * @package Buildwall
 */

add_action( 'after_setup_theme', 'buildwall_register_image_sizes', 5 );
/**
 * Register image sizes.
 */
function buildwall_register_image_sizes() {
	set_post_thumbnail_size( 360, 203, true );

	// Registers a new image sizes.
	add_image_size( 'buildwall-thumb-s', 150, 150, true );
	add_image_size( 'buildwall-thumb-m', 460, 460, true );
	add_image_size( 'buildwall-thumb-l', 870, 489, true );
	add_image_size( 'buildwall-thumb-l-2', 766, 203, true );
	add_image_size( 'buildwall-thumb-xl', 1170, 658, true );

	add_image_size( 'buildwall-thumb-masonry', 560, 9999 );

	add_image_size( 'buildwall-slider-thumb', 150, 86, true );

	add_image_size( 'buildwall-woo-cart-product-thumb', 104, 104, true );
	add_image_size( 'buildwall-thumb-listing-line-product', 370, 370, true );

	add_image_size( 'buildwall-thumb-75-75', 75, 75, true );
	add_image_size( 'buildwall-thumb-100-76', 100, 76, true );
	add_image_size( 'buildwall-thumb-240-131', 240, 131, true );
	add_image_size( 'buildwall-thumb-270-153', 270, 153, true );
	add_image_size( 'buildwall-thumb-270-203', 270, 203, true );
	add_image_size( 'buildwall-thumb-270-270', 270, 270, true );
	add_image_size( 'buildwall-thumb-370-370', 370, 370, true );
	add_image_size( 'buildwall-thumb-370-278', 370, 278, true );
	add_image_size( 'buildwall-thumb-420-316', 420, 316, true );
	add_image_size( 'buildwall-thumb-480-271', 480, 271, true );
	add_image_size( 'buildwall-thumb-480-361', 480, 361, true );
	add_image_size( 'buildwall-thumb-470-470', 470, 470, true );
	add_image_size( 'buildwall-thumb-480-695', 480, 695, true );
	add_image_size( 'buildwall-thumb-570-321', 570, 321, true );
	add_image_size( 'buildwall-thumb-570-428', 570, 428, true );
	add_image_size( 'buildwall-thumb-770-578', 770, 578, true );
	add_image_size( 'buildwall-thumb-1170-700', 1170, 700, true );
}
