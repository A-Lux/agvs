<?php
/**
 * Skins configuration.
 *
 * @package Buildwall
 */

function buildwall_skins_import_config() {
	return apply_filters( 'buildwall_skins_import_config', array(
		'default' => array(
			'label'    => esc_html__( 'Buildwall', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/default/default.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/default/default-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/default',
		),
		'skin-1' => array(
			'label'    => esc_html__( 'Road Construction', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/road-construction/road-construction.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/road-construction/road-construction-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/road',
		),
		'skin-2' => array(
			'label'    => esc_html__( 'House Lifting & Moving', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/house-lifting-moving/house-lifting-moving.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/house-lifting-moving/house-lifting-moving-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/moving',
		),
		'skin-3' => array(
			'label'    => esc_html__( 'Architect', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/architect/architect.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/architect/architect-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/architect',
		),
		'skin-4' => array(
			'label'    => esc_html__( 'Business', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/business/business.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/business/business-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/business',
		),
		'skin-5' => array(
			'label'    => esc_html__( 'Corporate', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/corporate/corporate.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/corporate/corporate-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/corporate',
		),
		'skin-6' => array(
			'label'    => esc_html__( 'Structon', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/structon/structon.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/structon/structon-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/structon',
		),
		'skin-7' => array(
			'label'    => esc_html__( 'Architect v2', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/architect-v2/architect-v2.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/architect-v2/architect-v2-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/architect2',
		),
		'skin-8' => array(
			'label'    => esc_html__( 'House lifting moving v2', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/house-lifting-moving-v2/house-lifting-moving-v2.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/house-lifting-moving-v2/house-lifting-moving-v2-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/moving2',
		),
		'skin-9' => array(
			'label'    => esc_html__( 'Pool design and service', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/pool/pool.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/pool/pool-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/pool',
		),
		'skin-10' => array(
			'label'    => esc_html__( 'Home Rent of construction equipment', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/equipment-rental/equipment-rental.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/equipment-rental/equipment-rental-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/equipment/',
		),
		'skin-11' => array(
			'label'    => esc_html__( 'Eco Construction', 'buildwall' ),
			'full'     => get_template_directory() . '/assets/demo-content/eco-construction/eco-construction.xml',
			'lite'     => false,
			'thumb'    => get_template_directory_uri() . '/assets/demo-content/eco-construction/eco-construction-thumb.png',
			'demo_url' => 'https://ld-wp.template-help.com/wordpress_prod-11024/v3/eco/',
		),
	) );
}