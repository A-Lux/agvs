<?php
/**
 * TM-Wizard configuration.
 *
 * @var array
 *
 * @package Buildwall
 */

$plugins = array(
	'cherry-data-importer' => array(
		'name'   => esc_html__( 'Cherry Data Importer', 'buildwall' ),
		'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
		'path'   => 'https://github.com/CherryFramework/cherry-data-importer/archive/master.zip',
		'access' => 'base',
	),
	'cherry-projects' => array(
		'name'   => esc_html__( 'Cherry Projects', 'buildwall' ),
		'access' => 'skins',
	),
	'cherry-team-members' => array(
		'name'   => esc_html__( 'Cherry Team Members', 'buildwall' ),
		'access' => 'skins',
	),
	'cherry-testi' => array(
		'name'   => esc_html__( 'Cherry Testimonials', 'buildwall' ),
		'access' => 'skins',
	),
	'cherry-search' => array(
		'name'   => esc_html__( 'Cherry Search', 'buildwall' ),
		'access' => 'skins',
	),
	'cherry-services-list' => array(
		'name'   => esc_html__( 'Cherry Services List', 'buildwall' ),
		'access' => 'skins',
	),
	'cherry-sidebars' => array(
		'name'   => esc_html__( 'Cherry Sidebars', 'buildwall' ),
		'access' => 'skins',
	),
	'cherry-trending-posts' => array(
		'name'   => esc_html__( 'Cherry Trending Posts', 'buildwall' ),
		'access' => 'skins',
	),
	'jet-menu' => array(
		'name'   => esc_html__( 'Jet menu', 'buildwall' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( '/assets/includes/plugins/jet-menu.zip' ),
		'access' => 'base',
	),
    'elementor' => array(
        'name'   => esc_html__( 'Elementor Page Builder', 'buildwall' ),
        'access' => 'base',
    ),
	'jet-elements' => array(
		'name'   => esc_html__( 'Jet Elements addon For Elementor', 'buildwall' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path(  '/assets/includes/plugins/jet-elements.zip' ),
		'access' => 'base',
	),
	'tm-photo-gallery' => array(
		'name'   => esc_html__( 'TM Photo Gallery', 'buildwall' ),
		'access' => 'skins',
	),
	'tm-timeline' => array(
		'name'   => esc_html__( 'TM Timeline', 'buildwall' ),
		'access' => 'skins',
	),
	'contact-form-7' => array(
		'name'   => esc_html__( 'Contact Form 7', 'buildwall' ),
		'access' => 'skins',
	),
	'simple-file-downloader' => array(
		'name'   => esc_html__( 'Simple File Downloader', 'buildwall' ),
		'access' => 'skins',
	),
	'shortcode-widget' => array(
		'name'   => esc_html__( 'Shortcode Widget', 'buildwall' ),
		'access' => 'skins',
	),
	'woocommerce' => array(
		'name'   => esc_html__( 'Woocommerce', 'buildwall' ),
		'access' => 'skins',
	),
	'tm-woocommerce-ajax-filters' => array(
		'name'   => esc_html__( 'TM Woocommerce Ajax Filters', 'buildwall' ),
		'source' => 'remote',
		'path'   => 'https://github.com/ZemezLab/tm-woocommerce-ajax-filters/archive/master.zip',
		'access' => 'skins',
	),
	'tm-woocommerce-compare-wishlist' => array(
		'name'   => esc_html__( 'TM Woocommerce Compare Wishlist', 'buildwall' ),
		'access' => 'skins',
	),
	'tm-woocommerce-package' => array(
		'name'   => esc_html__( 'TM Woocommerce Package', 'buildwall' ),
		'access' => 'skins',
	),
	'tm-woocommerce-quick-view' => array(
		'name'   => esc_html__( 'TM WooCommerce Quick View', 'buildwall' ),
		'source' => 'local',
		'path'   => get_parent_theme_file_path( '/assets/includes/plugins/tm-woocommerce-quick-view.zip' ),
		'access' => 'skins',
	),
	'rev-slider' => array(
		'name'   => esc_html__( 'Revolution Slider', 'buildwall' ),
		'source' => 'local',
		'path'   =>get_parent_theme_file_path( '/assets/includes/plugins/rev-slider.zip' ),
		'access' => 'skins',
	),
	'tm-dashboard' => array(
		'name'   => esc_html__( 'TM Dashboard', 'buildwall' ),
		'source' => 'remote',
		'path'   => 'http://cloud.cherryframework.com/downloads/free-plugins/tm-dashboard.zip',
		'access' => 'base',
	),
	'wordpress-social-login' => array(
		'name'   => esc_html__( 'WordPress Social Login', 'buildwall' ),
		'access' => 'skins',
	),
);

$full_plugins_list = array(
	'cherry-projects',
	'cherry-search',
	'cherry-services-list',
	'cherry-sidebars',
	'cherry-team-members',
	'cherry-testi',
	'cherry-trending-posts',
	'tm-photo-gallery',
	'tm-timeline',
	'contact-form-7',
	'simple-file-downloader',
	'rev-slider',
	'shortcode-widget',
	'jet-menu',
	'woocommerce',
	'tm-woocommerce-ajax-filters',
	'tm-woocommerce-compare-wishlist',
	'tm-woocommerce-package',
	'tm-woocommerce-quick-view',
	'wordpress-social-login',
);

$skins_import_config  = buildwall_skins_import_config();
$skins_plugins_config = array();

foreach ( $skins_import_config as $skin => $config ) {

	$skins_plugins_config[ $skin ] = array(
		'full'  => $full_plugins_list,
		'lite'  => false,
		'demo'  => $config['demo_url'],
		'thumb' => $config['thumb'],
		'name'  => $config['label'],
	);

}

/**
 * Skins configuration.
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'cherry-data-importer',
		'tm-dashboard',
		'elementor',
		'jet-elements',
		'jet-menu',

	),
	'advanced' => $skins_plugins_config,
);

$texts = array(
	'theme-name' => esc_html__( 'BUILDWALL', 'buildwall' ),
);
