<?php
/**
 * Pricing table heading template
 */
?>
<div class="pricing-table__heading">
	<?php $this->__html( 'icon', '<div class="pricing-table__icon"><div class="pricing-table__icon-box"><i class="%s"></i></div></div>' ); ?>
	<?php $this->__html( 'title', '<h6 class="pricing-table__title">%s</h6>' ); ?>
	<?php $this->__html( 'subtitle', '<em class="pricing-table__subtitle">%s</em>' ); ?>
	<?php $this->__glob_inc_if( 'price', array( 'price_prefix', 'price', 'price_suffix' ) ); ?>
</div>
