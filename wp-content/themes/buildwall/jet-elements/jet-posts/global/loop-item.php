<?php
/**
 * Posts loop start template
 */
?>
<div class="jet-posts__item <?php echo jet_elements_tools()->col_classes( array(
	'desk' => $this->get_attr( 'columns' ),
	'tab'  => $this->get_attr( 'columns_tablet' ),
	'mob'  => $this->get_attr( 'columns_mobile' ),
) ); ?>">
	<div class="jet-posts__inner-box"<?php $this->add_box_bg(); ?>><?php

		include $this->get_template( 'item-thumb' );

		echo '<div class="jet-posts__inner-content">';

			if ( 'background' == $this->get_attr( 'show_image_as' ) ) {

				?><div class="hover-style-2"><div class="overflow-content"><div class="posts-header-content"><?php
					jet_elements()->utility()->meta_data->get_terms( array(
						'type'      => 'category',
						'delimiter' => false,
						'prefix'    => false,
						'before'    => '<span class="post__cats">',
						'after'     => '</span>',
						'echo'      => true,
					) );
				?></div>
					<div class="posts-middle-content"><?php
					include $this->get_template( 'item-title' );
					include $this->get_template( 'item-content' );
				?></div>
					<div class="posts-footer-content"><?php
					include $this->get_template( 'item-more' );
				?></div>
					<div class="posts-footer-content-hover"><?php
					include $this->get_template( 'item-meta' );
				?></div></div></div><?php

			} else {
				include $this->get_template( 'item-title' );
				include $this->get_template( 'item-meta' );
				include $this->get_template( 'item-content' );
				include $this->get_template( 'item-more' );
			}

		echo '</div>';

	?></div>
</div>