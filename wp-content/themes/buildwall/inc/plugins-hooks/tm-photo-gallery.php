<?php
/**
 * Tm-photo-gallery hooks.
 *
 * @package Buildwall
 */

// Customization tm-photo-gallery plugin.
add_filter( 'theme_mod_sidebar_position', 'buildwall_gallery_single_sidebar_position' );

add_filter( 'theme_mod_content_container_type', 'buildwall_gallery_single_content_fullwidth' );

add_action('init', 'buildwall_pg_add_imeges_size_filter');

/**
 * Disable sidebar to single gallery albums & sets.
 */
function buildwall_gallery_single_sidebar_position( $value ) {

	if ( is_singular( 'tm_pg_album' ) || is_singular('tm_pg_set') ) {
		return 'fullwidth';
	}

	return $value;
}

/**
 * Fullwidth single gallery albums & sets.
 */
function buildwall_gallery_single_content_fullwidth( $value ) {

	if ( is_singular( 'tm_pg_album' ) || is_singular('tm_pg_set') ) {
		return 'fullwidth';
	}

	return $value;
}

/**
 * buildwall_pg_add_imeges_size_filter
 */
function buildwall_pg_add_imeges_size_filter() {
	add_filter( 'tm_pg_get_sizes', 'buildwall_pg_images_sizes' );
}

/**
 * buildwall_images_sizes
 *
 * @param  $args   The arguments
 *
 * @return   ( description_of_the_return_value )
 */
function buildwall_pg_images_sizes( $args ) {
	$args['grid-default'] = array(
		'width'   => '640',
		'height'  => '482',
		'type'   => 'grid',
		);
	return $args;
}