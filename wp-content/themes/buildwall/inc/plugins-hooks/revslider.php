<?php
/**
 * Revslider hooks.
 *
 * @package Buildwall
 */
// Add builder dynamic files.
add_filter( 'buildwall_get_dynamic_css_options', 'buildwall_add_revslider_dynamic_file' );

/**
 * Add builder dynamic files.
 */
function buildwall_add_revslider_dynamic_file( $options ) {

	$dynamic_files = array(
		BUILDWALL_THEME_DIR . '/assets/css/dynamic/plugins/revslider.css',
	);

	$options['css_files'] = array_merge( $options['css_files'], $dynamic_files );

	return $options;
}
