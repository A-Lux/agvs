<?php
/**
 * Cherry-projects hooks.
 *
 * @package Buildwall
 */

// Add single widget area
add_filter( 'buildwall_widget_area_default_settings', 'buildwall_add_cherry_projects_single_widget_area' );

// Customization cherry-project plugin.
add_filter( 'cherry-projects-title-settings', 'buildwall_cherry_projects_title_settings' );
add_filter( 'cherry-projects-author-settings', 'buildwall_cherry_projects_author_settings' );
add_filter( 'cherry-projects-button-settings', 'buildwall_cherry_projects_button_settings' );
add_filter( 'cherry-projects-content-settings', 'buildwall_cherry_projects_content_settings' );
add_filter( 'cherry_projects_show_all_text', 'buildwall_projects_show_all_text' );
add_filter( 'cherry-projects-prev-button-text', 'buildwall_cherry_projects_prev_button_text' );
add_filter( 'cherry-projects-next-button-text', 'buildwall_cherry_projects_next_button_text' );
add_filter( 'cherry_projects_data_callbacks', 'buildwall_cherry_projects_data_callbacks', 10, 2 );
add_filter( 'cherry_projects_cascading_list_map', 'buildwall_cherry_projects_cascading_list_map' );
add_filter( 'cherry-projects-terms-list-settings', 'buildwall_modify_cherry_projects_terms_list_settings' );
add_filter( 'cherry-projects-comments-settings', 'buildwall_modify_cherry_projects_comments_settings' );
add_filter( 'cherry-projects-details-list-text', 'buildwall_modify_cherry_projects_details_text' );
add_filter( 'cherry_projects_metabox_fields_settings', 'buildwall_cherry_projects_metabox_fields_settings' );


/**
 * Add single widget area.
 */
function buildwall_add_cherry_projects_single_widget_area( $areas ) {

	$areas['single-project'] = array(
		'name'           => esc_html__( 'Single Projects Area', 'buildwall' ),
		'description'    => esc_html__( 'Display only at single projects pages', 'buildwall' ),
		'before_widget'  => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'   => '</aside>',
		'before_title'   => '<h5 class="widget-title title-decoration">',
		'after_title'    => '</h5>',
		'before_wrapper' => '<section id="%1$s" %2$s>',
		'after_wrapper'  => '</section>',
		'is_global'      => true,
	);

	return $areas;
}

/**
 * Customization title settings to cherry-project.
 *
 * @param array $settings Title settings.
 *
 * @return array
 */
function buildwall_cherry_projects_title_settings( $settings ) {

	$title_html = ( is_single() ) ? '<h3 class="project-entry-title">%4$s</h3>' : '<h5 %1$s><a href="%2$s" %3$s rel="bookmark">%4$s</a></h5>';

	$settings['html']  = $title_html;
	$settings['class'] = 'project-entry-title title-decoration';

	if ( is_single() ) {
		$settings['length'] = - 1;
	}

	return $settings;
}

/**
 * Customization meta author settings to cherry-project.
 *
 * @param array $settings Author settings.
 *
 * @return array
 */
function buildwall_cherry_projects_author_settings( $settings ) {

	$settings['html']   = '<span class="posted-by">%1$s<a href="%2$s" %3$s %4$s rel="author">%5$s%6$s</a></span>';
	$settings['prefix'] = esc_html__( 'by ', 'buildwall' );

	return $settings;
}

/**
 * Customization button settings to cherry-project.
 *
 * @param array $settings Button settings.
 *
 * @return array
 */
function buildwall_cherry_projects_button_settings( $settings ) {

	$new_settings = array(
		'text'  => esc_html__( 'Learn more', 'buildwall' ),
		'class' => 'project-more-button btn-link',
		'html'  => '<a href="%1$s" %3$s><span class="btn__text">%4$s</span>%5$s</a>',
	);

	$settings = array_merge( $settings, $new_settings );

	return $settings;
}

/**
 * Customization content settings to cherry-project.
 *
 * @param array $settings Content settings.
 *
 * @return array
 */
function buildwall_cherry_projects_content_settings( $settings ) {

	$settings['class'] = 'project-entry-content';

	return $settings;
}

/**
 * Customization show all text to cherry-project.
 *
 * @return string
 */
function buildwall_projects_show_all_text( $show_all_text ) {
	return esc_html__( 'All', 'buildwall' );
}

/**
 * Customization cherry-projects prev button text.
 *
 * @return string
 */
function buildwall_cherry_projects_prev_button_text( $prev_text ) {
	return sprintf( '%s %s', '<i class="nc-icon-mini arrows-1_minimal-left"></i>', esc_html__( 'PREV', 'buildwall' ) );
}

/**
 * Customization cherry-projects next button text.
 *
 * @return string
 */
function buildwall_cherry_projects_next_button_text( $next_text ) {

	return sprintf( '%s %s', esc_html__( 'NEXT', 'buildwall' ), '<i class="nc-icon-mini arrows-1_minimal-right"></i>' );
}
/**
 * Add macroses to cherry-project.
 *
 * @return array
 */
function buildwall_cherry_projects_data_callbacks( $data, $atts ) {

	$data['sharebuttons']  = 'buildwall_get_single_share_buttons';
	$data['date']          = 'buildwall_modify_cherry_projects_get_date';
	$data['content_description'] = 'buildwall_get_projects_content_description';
	$data['skills_title'] = 'buildwall_get_projects_skills_title';
	$data['externallink_button'] = 'buildwall_get_external_link_button_primary';

	return $data;
}

/**
 * Get skills title.
 *
 * @return array
 */
function buildwall_get_external_link_button_primary( $attr = array() ) {

	$attr = wp_parse_args( $attr );

	$external_link_text = get_post_meta( get_the_ID(), 'cherry_projects_external_link_text', true );
	$external_link = get_post_meta( get_the_ID(), 'cherry_projects_external_link', true );
	$external_target = get_post_meta( get_the_ID(), 'cherry_projects_external_link_target', true );

	$html = '';

	if ( ! empty( $external_link ) ) {
		$html = sprintf( '<a class="external-link-button-primary btn btn-primary" href="%1$s" target="%2$s"><i class="nc-icon-mini ui-2_link-68"></i>%3$s</a>',
			! empty( $external_link ) ? $external_link : '#',
			! empty( $external_target ) ? '_' . $external_target : '_blank',
			! empty( $external_link_text ) ? $external_link_text : 'external link'
		);
	}

	return $html;
}

/**
 * Get skills title.
 *
 * @return array
 */
function buildwall_get_projects_skills_title() {
	$format = '<h5 class="title-decoration">%s</h5>';

	return sprintf( $format, esc_html__( 'Project skills', 'buildwall' ) );
}

/**
 * Create field in dashboard for description description.
 *
 * @return array
 */
function buildwall_cherry_projects_metabox_fields_settings ( $args ) {
	$args['fields']['cherry-projects_content_description'] = array(
		'type'              => 'textarea',
		'parent'            => 'general_tab',
		'title'             => esc_html__( 'Short Description', 'buildwall' ),
		'description'       => esc_html__( 'Short Description', 'buildwall' ),
	);
	return $args;

}
/**
 * Get content from field description.
 *
 * @return array
 */
function buildwall_projects_description( $attr = array() ) {

	$attr = wp_parse_args( $attr );

	$descr = get_post_meta( get_the_ID(), 'cherry-projects_content_description', true );

	$html = '';

	if ( !is_single() ) {
		$trimmed_content = wp_trim_words( $descr, 20, '...' );
	} else {
		$trimmed_content = $descr;
	}

	if ( ! empty( $descr ) ) {
		$html = sprintf( '<div class="cherry-projects-single-description"><h5 class="title-decoration">%2$s</h5><p>%1$s</p></div>', $trimmed_content, esc_html__( 'Description', 'buildwall' ));
	}

	return $html;
}

/**
 * Get content description.
 *
 * @return array
 */
function buildwall_get_projects_content_description() {
	return buildwall_projects_description();
}

/**
 * Customization cherry-projects cascading list map.
 *
 * @return array
 */
function buildwall_cherry_projects_cascading_list_map( $cascading_list_map ) {
	return array( 2, 2, 3, 3, 3, 4, 4, 4, 4 );
}

/**
 * Customization cherry-projects date.
 *
 * @return array
 */
function buildwall_modify_cherry_projects_get_date( $attr = array() ) {

	$utility      = buildwall_utility()->utility;
	$default_attr = array( 'format' => 'F, j Y', 'human_time' => false );

	$attr = wp_parse_args( $attr, $default_attr );

	$settings = array(
		'visible'		=> true,
		'icon'			=> '',
		'prefix'		=> '',
		'html'			=> '%1$s<a href="%2$s" %3$s %4$s ><time datetime="%5$s" title="%5$s">%6$s%7$s</time></a>',
		'title'			=> '',
		'class'			=> 'post-date',
		'date_format'	=> $attr['format'],
		'human_time'	=> filter_var( $attr['human_time'] , FILTER_VALIDATE_BOOLEAN ),
		'echo'			=> false,
	);

	/**
	 * Filter post date settings.
	 *
	 * @since 1.0.0
	 * @var array
	 */
	$settings = apply_filters( 'buildwall-cherry-projects-date-settings', $settings );

	if ( is_single() ) {
		$date = $utility->meta_data->get_date( array(
			'html'        => '<div class="post__date post__date-circle"><a href="%2$s" %3$s %4$s ><time datetime="%5$s">',
			'class'       => 'post__date-link',
			'echo'        => false,
		) );

		$date .= $utility->meta_data->get_date( array(
			'date_format' => 'd',
			'html'        => '<span class="post__date-day">%6$s%7$s</span>',
			'class'       => 'post__date-link',
			'echo'        => false,
		) );

		$date .= $utility->meta_data->get_date( array(
			'date_format' => 'M',
			'html'        => '<span class="post__date-month">%6$s%7$s</span>',
			'class'       => 'post__date-link',
			'echo'        => false,
		) );

		$date .= $utility->meta_data->get_date( array(
			'html'        => '</time></a></div>',
			'class'       => 'post__date-link',
			'echo'        => false,
		) );
	} else {
		$date = $utility->meta_data->get_date( $settings );
	}

	return $date;
}

/**
 * @param array $settings
 *
 * @return array
 */
function buildwall_modify_cherry_projects_terms_list_settings( $settings = array() ) {

	$settings['before'] = '<span class="post-terms post__cats">';
	$settings['after']  = '</span>';
	$settings['prefix'] = false;

	if ( 'projects_tag' === $settings[ 'type' ] ){
		$settings[ 'prefix' ] = esc_html__( 'Tags: ', 'buildwall' );
	}

	return $settings;
}

/**
 * @param array $settings
 *
 * @return array
 */
function buildwall_modify_cherry_projects_comments_settings( $settings ) {

	$settings['icon']  = '<i class="nc-icon-mini ui-2_chat-round-content"></i>';
	$settings['class'] = 'post-comments-count post__comments';
	$settings['sufix'] = '%s';

	return $settings;
}

/**
 * @param string $text
 *
 * @return string
 */
function buildwall_modify_cherry_projects_details_text( $text ) {
	return esc_html__( 'Details', 'buildwall' );
}
