<?php
/**
 * Cherry Team Members hooks.
 *
 * @package Buildwall
 */

// Add template to cherry team-memebers templates list;
add_filter( 'cherry_team_templates_list', 'buildwall_cherry_team_templates_list' );

// Customization cherry-team plugin.
add_filter( 'cherry_team_members_meta_args', 'buildwall_modify_team_members_meta_options' );

// Customization cherry-team plugin social icons.
add_filter( 'cherry_team_social_icon_format', 'buildwall_cherry_team_social_icon_format' );

// Add `Cover image` meta field.
add_filter( 'cherry_team_members_meta_args', 'buildwall_cherry_team_members_meta_args' );

// Add macros %%COVERIMAGE%% and callback function.
add_filter( 'cherry_team_data_callbacks', 'buildwall_cherry_team_data_callbacks' );

// Modify heading format.
add_filter( 'cherry_team_shortcode_heading_format', 'buildwall_modify_cherry_team_shortcode_heading_format' );


/**
 *  Add template to cherry team-memebers templates list;
 */
function buildwall_cherry_team_templates_list( $tmpl_list ) {
	$tmpl_list['list']            = 'list.tmpl';
	$tmpl_list['info']            = 'info.tmpl';
	return $tmpl_list;
}


/**
 * Add `Cover image` meta field.
 *
 * @param array $args Meta args.
 *
 * @return array
 */
function buildwall_cherry_team_members_meta_args( $args = array() ) {

	$new_args = array(
		'cherry-team-email' => array(
			'type'              => 'text',
			'placeholder'       => esc_html__( 'E-mail', 'buildwall' ),
			'label'             => esc_html__( 'E-mail', 'buildwall' ),
			'sanitize_callback' => 'sanitize_email'
		),
	);

	buildwall_array_insert( $args['fields'], 3, $new_args );
	return $args;
}

/**
 * Modify cherry-services-list meta options.
 */
function buildwall_modify_team_members_meta_options( $fields ) {

	// Change icon data.
	$fields['fields']['cherry-team-social']['fields']['icon']['icon_data'] = buildwall_get_nc_mini_icons_data();

	return $fields;
}

/**
 * Change cherry-services-list icon format
 *
 * @return string
 */
function buildwall_cherry_team_social_icon_format( $icon_format ) {
	return '<i class="nc-icon-mini %s"></i>';
}

/**
 * Add macros %%EMAIL%%, %%TITLE%%, %%ICON%% and callback function.
 *
 * @param array $data Item data.
 *
 * @return array
 */
function buildwall_cherry_team_data_callbacks( $data = array() ) {

	$data['email']          = 'buildwall_get_cherry_team_email';
	$data['icon']           = 'buildwall_get_cherry_team_icon';
	$data['title_phone']    = 'buildwall_get_cherry_team_title_phone';
	$data['title_location'] = 'buildwall_get_cherry_team_title_location';
	$data['title_email']    = 'buildwall_get_cherry_team_title_email';
	$data['content_title']  = 'buildwall_get_cherry_team_content_title';

	return $data;
}

/**
 * Callback function for macros %%EMAIL%%.
 */
function buildwall_get_cherry_team_email() {

	global $post;
	$email = get_post_meta( $post->ID, 'cherry-team-email', true );

	if ( ! $email ) {
		return '';
	}

	$format = apply_filters( 'buildwall_cherry_team_email_format', '<span class="team-email"><a href="mailto:%1$s">%1$s</a></span>' );

	return sprintf( $format, $email );
}

/**
 * Callback function for macros %%ICON%%.
 */
function buildwall_get_cherry_team_icon( $args = array() ) {

	if ( isset( $args['icon'] ) && false === $args['icon'] ) {
		return;
	}

	if ( isset( $args['for'] ) && false === $args['for'] ) {
		return;
	}

	global $post;
	$meta = $args['for'];
	$value = get_post_meta( $post->ID, 'cherry-team-' . $meta, true );

	if ( empty( $value ) ) {
		return;
	}

	$format = apply_filters( 'buildwall_cherry_team_icon_format', '<span class="team-icon"><i class="%s"></i></span>' );

	return sprintf( $format, $args['icon'] );
}

/**
 * Callback function for macros %%TITLE_PHONE%%.
 */
function buildwall_get_cherry_team_title_phone() {
	global $post;
	$value = get_post_meta( $post->ID, 'cherry-team-phone', true );

	if ( empty( $value ) ) {
		return;
	}

	$format = apply_filters( 'buildwall_cherry_team_phone_title_format', '<h5 class="team-meta-title">%s</h5>' );

	return sprintf( $format, esc_html__( 'Phones:', 'buildwall' ) );
}

/**
 * Callback function for macros %%TITLE_LOCATION%%.
 */
function buildwall_get_cherry_team_title_location() {
	global $post;
	$value = get_post_meta( $post->ID, 'cherry-team-location', true );

	if ( empty( $value ) ) {
		return;
	}

	$format = apply_filters( 'buildwall_cherry_team_location_title_format', '<h5 class="team-meta-title">%s</h5>' );

	return sprintf( $format, esc_html__( 'Address Info', 'buildwall' ) );
}

/**
 * Callback function for macros %%TITLE_PHONE%%.
 */
function buildwall_get_cherry_team_title_email() {
	global $post;
	$value = get_post_meta( $post->ID, 'cherry-team-email', true );

	if ( empty( $value ) ) {
		return;
	}

	$format = apply_filters( 'buildwall_cherry_team_email_title_format', '<h5 class="team-meta-title">%s</h5>' );

	return sprintf( $format, esc_html__( 'E-mail:', 'buildwall' ) );
}

/**
 * Callback function for macros %%CONTENT_TITLE%%.
 */
function buildwall_get_cherry_team_content_title() {
	$format = '<h5 class="post-content__title">%s</h5>';

	return sprintf( $format, esc_html__( 'PROFILE', 'buildwall' ) );
}

/**
 * Modify heading format.
 *
 * @param array $format Heading formats.
 *
 * @return array
 */
function buildwall_modify_cherry_team_shortcode_heading_format( $format = array() ) {

	$format['super_title'] = '<h6 class="team-heading_super_title">%s</h6>';
	$format['subtitle']    = '<h5 class="team-heading_subtitle">%s</h5>';

	return $format;
}

/**
 * Array insert function.
 *
 * @return array
 */
function buildwall_array_insert( &$array, $position, $insert_array ) {
	$first_array = array_splice( $array, 0, $position );
	$array = array_merge( $first_array, $insert_array, $array );
}