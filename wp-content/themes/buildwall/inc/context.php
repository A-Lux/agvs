<?php
/**
 * Contextual functions for the header, footer, content and sidebar classes.
 *
 * @package Buildwall
 */

/**
 * Contain utility module from Cherry framework
 *
 * @since  1.0.0
 * @return object
 */
function buildwall_utility() {
	return buildwall_theme()->get_core()->modules['cherry-utility'];
}

/**
 * Get html attribute - class.
 *
 * @param array  $classes      CSS classes.
 * @param string $color_option Customizer color option.
 *
 * @return string
 */
function buildwall_get_html_attr_class( $classes = array(), $color_option = null ) {

	if ( $color_option ) {
		$color = get_theme_mod( $color_option, buildwall_theme()->customizer->get_default( $color_option ) );

		if ( 'dark' === buildwall_hex_color_is_light_or_dark( $color ) && $color ) {
			$classes[] = 'invert';
		}
	}

	return 'class="' . join( ' ', $classes ) . '"';
}

/**
 * Prints site header CSS classes.
 *
 * @since  1.0.0
 * @param  array $classes Additional classes.
 * @return void
 */
function buildwall_header_class( $classes = array() ) {
	$classes[] = 'site-header';
	$classes[] = buildwall_get_mod( 'header_layout_type', true, 'sanitize_html_class' );

	if ( buildwall_get_mod( 'header_btn_visibility', true, 'esc_attr' )
		&& buildwall_get_mod( 'header_btn_text', true, 'esc_attr' )
		&& buildwall_get_mod( 'header_btn_url', true, 'esc_url' )
	) {
		$classes[] = 'header-btn-visibility';
	}

	if ( buildwall_get_mod( 'header_transparent_layout', true, 'sanitize_html_class' ) ) {
		$classes[] = 'transparent';
	}

	echo buildwall_get_container_classes( $classes, 'header' );
}

/**
 * Prints site header container CSS classes
 *
 * @since   1.0.0
 * @param   array $classes Additional classes.
 * @return  void
 */
function buildwall_header_container_class( $classes = array() ) {
	$classes[] = 'header-container';

	if ( buildwall_get_mod( 'header_transparent_layout', true, 'sanitize_html_class' ) ) {
		$classes[] = 'transparent';
	}

	if ( buildwall_get_mod( 'header_invert_color_scheme', true, 'sanitize_html_class' ) ) {
		$classes[] = 'invert';
	}

	echo 'class="' . join( ' ', $classes ) . '"';
}

/**
 * Prints site content CSS classes.
 *
 * @since  1.0.0
 * @param  array $classes Additional classes.
 * @return void
 */
function buildwall_content_class( $classes = array() ) {
	$classes[] = 'site-content';

	echo 'class="' . join( ' ', $classes ) . '"';
}

/**
 * Prints site content wrap CSS classes.
 *
 * @since  1.0.0
 * @param  array $classes Additional classes.
 * @return void
 */
function buildwall_content_wrap_class( $classes = array() ) {
	$classes[] = 'site-content_wrap';

	echo buildwall_get_container_classes( $classes, 'content' );
}

/**
 * Prints site footer CSS classes.
 *
 * @since  1.0.0
 * @param  array $classes Additional classes.
 * @return void
 */
function buildwall_footer_class( $classes = array() ) {
	$classes[] = 'site-footer';
	$classes[] = buildwall_get_mod( 'footer_layout_type', true, 'sanitize_html_class' );

	if ( buildwall_widget_area()->is_active_sidebar( 'after-content-full-width-area' ) ) {
		$classes[] = 'before-full-width-area';
	}

	echo buildwall_get_container_classes( $classes, 'footer' );
}

/**
 * Prints footer container CSS classes.
 *
 * @since  1.0.0
 * @param  array $classes Additional classes.
 * @return void
 */
function buildwall_footer_container_class( $classes = array() ) {
	$classes[] = 'footer-container';

	$footer_bg          = buildwall_get_mod( 'footer_bg', true, 'sanitize_html_class' );
	$footer_layout_type = buildwall_get_mod( 'footer_layout_type', true, 'sanitize_html_class' );



	if ( 'dark' === buildwall_hex_color_is_light_or_dark( $footer_bg ) ) {
		$classes[] = 'invert';
	}

	echo 'class="' . join( ' ', $classes ) . '"';
}

/**
 * Retrieve a CSS class attribute for container based on `Page Layout Type` option.
 *
 * @since  1.0.0
 * @param  array  $classes Additional classes.
 * @param  string $target
 * @return string
 */
function buildwall_get_container_classes( $classes, $target = 'content' ) {

	$layout_type = get_theme_mod( $target . '_container_type' );

	if ( 'boxed' == $layout_type ) {
		$classes[] = 'container';
	}

	return 'class="' . join( ' ', $classes ) . '"';
}

/**
 * Prints primary content wrapper CSS classes.
 *
 * @since  1.0.0
 * @param  array $classes Additional classes.
 * @return void
 */
function buildwall_primary_content_class( $classes = array() ) {
	echo buildwall_get_layout_classes( 'content', $classes );
}

/**
 * Get CSS class attribute for passed layout context.
 *
 * @since  1.0.0
 * @param  string $layout  Layout context.
 * @param  array  $classes Additional classes.
 * @return string
 */
function buildwall_get_layout_classes( $layout = 'content', $classes = array() ) {
	$sidebar_position = buildwall_get_mod( 'sidebar_position', true, 'esc_attr' );
	$sidebar_width = buildwall_get_mod( 'sidebar_width', true, 'esc_attr' );

	if ( 'fullwidth' === $sidebar_position ) {
		$sidebar_width = 0;
	}

	$layout_classes = ! empty( buildwall_theme()->layout[ $sidebar_position ][ $sidebar_width ][ $layout ] ) ? buildwall_theme()->layout[ $sidebar_position ][ $sidebar_width ][ $layout ] : array();

	$layout_classes = apply_filters( "buildwall_{$layout}_classes", $layout_classes );

	if ( ! empty( $classes ) ) {
		$layout_classes = array_merge( $layout_classes, $classes );
	}

	if ( empty( $layout_classes ) ) {
		return '';
	}

	return 'class="' . join( ' ', $layout_classes ) . '"';
}

/**
 * Retrieve or print `class` attribute for Post List wrapper.
 *
 * @since  1.0.0
 * @param  array   $classes Additional classes.
 * @param  boolean $echo    True for print. False - return.
 * @return string|void
 */
function buildwall_posts_list_class( $classes = array(), $echo = true ) {
	$layout_type         = buildwall_get_mod( 'blog_layout_type', true, 'sanitize_html_class' );
	$layout_type         = ! is_search() ? $layout_type : 'search';
	$columns             = buildwall_get_mod( 'blog_layout_columns', true, 'sanitize_html_class' );
	$sidebar_position    = buildwall_get_mod( 'sidebar_position', true, 'sanitize_html_class' );
	$blog_content        = buildwall_get_mod( 'blog_posts_content', true, 'sanitize_html_class' );
	$blog_featured_image = buildwall_get_mod( 'blog_featured_image', true, 'sanitize_html_class' );

	$classes[] = 'posts-list';
	$classes[] = 'posts-list--' . $layout_type;
	$classes[] = 'content-' . $blog_content;
	$classes[] = $sidebar_position;

	if ( 'grid' === $layout_type ) {
		$classes[] = 'card-deck';
	}

	if ( 'masonry' === $layout_type ) {
		$classes[] = 'card-columns';
	}

	if ( in_array( $layout_type, array( 'grid', 'masonry' ) ) ) {
		$classes[] = sprintf( 'posts-list--%1$s-%2$s', sanitize_html_class( $layout_type ), sanitize_html_class( $columns ) );
	}

	if ( 'default' === $layout_type ) {
		$classes[] = sprintf( 'posts-list--%1$s-%2$s-image', sanitize_html_class( $layout_type ), sanitize_html_class( $blog_featured_image ) );
	}

	$sidebars = array(
		'full-width-header-area',
		'before-content-area',
		'before-loop-area',
	);

	$has_sidebars = false;

	foreach ( $sidebars as $sidebar ) {
		if ( buildwall_widget_area()->is_active_sidebar( $sidebar ) ) {
			$has_sidebars = true;
		}
	}

	if ( ! $has_sidebars && is_home() ) {
		$classes[] = 'no-sidebars-before';
	}

	$classes = apply_filters( 'buildwall_posts_list_class', $classes );

	$output = 'class="' . join( ' ', $classes ) . '"';

	if ( ! $echo ) {
		return $output;
	}

	echo wp_kses_post( $output );
}

/**
 * Check if product paage currently displaying
 *
 * @return bool
 */
function buildwall_is_product_page() {
	if ( ! function_exists( 'is_product' ) || ! function_exists( 'is_shop' ) || ! function_exists( 'is_product_taxonomy' ) ) {
		return false;
	}

	return is_product() || is_shop() || is_product_taxonomy();
}
