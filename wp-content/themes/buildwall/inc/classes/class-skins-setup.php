<?php
/**
 * Skins Setup.
 *
 * @package Buildwall
 */

if ( ! class_exists( 'Buildwall_Skins_Setup' ) ) {

	/**
	 * Class Buildwall_Skins_Setup.
	 */
	class Buildwall_Skins_Setup {

		/**
		 * A reference to an instance of this class.
		 *
		 * @since 1.0.0
		 * @var   object
		 */
		private static $instance = null;

		/**
		 * Default skins directory path
		 * @var string
		 */
		public $skins_path;

		/**
		 * Default skins directory
		 * @var string
		 */
		public $skins_dir;

		/**
		 * Skins settings.
		 * @var array
		 */
		public $skins_settings = array();

		/**
		 * Current skin.
		 * @var string.
		 */
		public $current_skin;
		/**
		 * Skin css handler.
		 * @var string
		 */
		public $skin_css_handler = 'buildwall-skin-style';

		/**
		 * Sets up needed actions/filters for the skins to initialize.
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			$this->skins_path     = apply_filters( 'buildwall_skins_directory_path', 'skins/' );
			$this->skins_dir      = trailingslashit( BUILDWALL_THEME_DIR ) . $this->skins_path;
			$this->skins_settings = $this->get_skins_settings();

			// Set current skin.
			$this->current_skin = $this->get_current_skin();

			// Change parent-handler value into dynamic css module.
			add_filter( 'buildwall_get_dynamic_css_options', array( $this, 'set_dynamic_parent_handler' ) );

			// Load the skin functions file.
			add_action( 'after_setup_theme', array( $this, 'include_skin_functions_file' ), 5 );

			// Additional body classes.
			add_filter( 'body_class', array( $this, 'extra_body_classes' ) );

			// Register skin assets.
			add_action( 'wp_enqueue_scripts', array( $this, 'register_assets' ), 9 );

			// Enqueue skin assets.
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ), 21 );

			// Include current skin dynamic css file.
			add_action( 'cherry_dynamic_css_include_custom_files', array( $this, 'include_skin_dynamic_css_file' ), 20, 2 );

			// Modify theme url path.
			add_filter( 'buildwall_render_theme_url_path', array( $this, 'modify_render_theme_url_path' ) );

		}

		/**
		 * Get skins settings.
		 *
		 * @return array
		 */
		public function get_skins_settings() {
			$skins  = array();
			$handle = opendir( $this->skins_dir );

			if ( ! $handle ) {
				return false;
			}

			while ( ( false !== $entry = readdir( $handle ) ) ) {
				if ( ! is_file( $entry ) && ! in_array( $entry, array( '.', '..' ) ) ) {
					$skins[] = $entry;
				}
			}

			closedir( $handle );

			$skins_settings = array();

			foreach ( $skins as $skin ) {
				$skin_dir        = trailingslashit( $this->skins_dir ) . $skin;
				$skin_style_file = trailingslashit( $skin_dir ) . 'style-skin.css';

				if ( file_exists( $skin_style_file ) ) {
					$slug      = strtolower( str_replace( ' ', '-', $skin ) );
					$skin_data = get_file_data( $skin_style_file, array(
						'name' => 'Skin Name',
					) );

					if ( $skin_data['name'] ) {
						$skins_settings[ $slug ] = array(
							'name'      => $skin_data['name'],
							'skin-path' => trailingslashit( $this->skins_path ) . $skin,
							'dir'       => $skin_dir,
							'uri'       => str_replace( WP_CONTENT_DIR, WP_CONTENT_URL, $skin_dir ),
						);
					}
				}
			}

			return $skins_settings;
		}

		/**
		 * Get skins options for 'Current skin' customizer control.
		 *
		 * @return array
		 */
		public function get_skins_select_options() {
			$options = array();

			foreach ( $this->skins_settings as $skin_slug => $skin_data ) {
				$options[ $skin_slug ] = $skin_data['name'];
			}

			return $options;
		}

		/**
		 * Get current skin slug.
		 *
		 * @return string
		 */
		public function get_current_skin() {
			return get_theme_mod( 'current_skin', 'default' );
		}

		/**
		 * Get current skin dir path.
		 *
		 * @return string
		 */
		public function get_current_skin_path() {
			return $this->skins_settings[ $this->current_skin ]['skin-path'];
		}

		/**
		 * Change parent-handler value into dynamic css module.
		 *
		 * @param array $args Dynamic css arguments.
		 *
		 * @return array
		 */
		public function set_dynamic_parent_handler( $args = array() ) {

			$args['parent_handle'] = $this->skin_css_handler;

			return $args;
		}

		/**
		 * Loads the skin functions file.
		 *
		 * @since 1.0.0
		 */
		public function include_skin_functions_file() {
			$skin_func_file = trailingslashit( $this->skins_settings[ $this->current_skin ]['dir'] ) . 'functions.php';

			if ( file_exists( $skin_func_file ) ) {
				require_once $skin_func_file;
			}
		}

		/**
		 * Additional body classes.
		 *
		 * @param array $classes
		 *
		 * @return array
		 */
		public function extra_body_classes( $classes = array() ) {

			$classes[] = sprintf( 'skin-%s', $this->current_skin );

			return $classes;
		}

		/**
		 * Register assets.
		 *
		 * @since 1.0.0
		 */
		public function register_assets() {
			$skin_style_file_url = trailingslashit( $this->skins_settings[ $this->current_skin ]['uri'] ) . 'style-skin.css';

			wp_register_style( $this->skin_css_handler, $skin_style_file_url, array( 'buildwall-theme-style' ), BUILDWALL_THEME_VERSION );
		}

		/**
		 * Enqueue assets.
		 *
		 * @since 1.0.0
		 */
		public function enqueue_assets() {
			wp_enqueue_style( $this->skin_css_handler );
		}

		/**
		 * Include current skin dynamic css file.
		 */
		public function include_skin_dynamic_css_file( $args, $core ) {
			$skin_dynamic_file_path = "assets/css/dynamic-{$this->current_skin}.css";
			$skin_dynamic_file      = trailingslashit( $this->skins_settings[ $this->current_skin ]['dir'] ) . $skin_dynamic_file_path;

			if ( file_exists( $skin_dynamic_file ) ) {
				include $skin_dynamic_file;
			}
		}

		/**
		 * Modify theme url path.
		 *
		 * @param string $path Theme url path.
		 *
		 * @return string
		 */
		public function modify_render_theme_url_path( $path ) {

			if ( 'default' !== $this->current_skin ) {
				$path .= '/' . $this->get_current_skin_path();
			}

			return $path;
		}

		/**
		 * Returns the instance.
		 *
		 * @since  1.0.0
		 * @return object
		 */
		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}

			return self::$instance;
		}
	}
} // End if().

/**
 * Returns instance of skins configuration class.
 *
 * @since  1.0.0
 * @return object
 */
function buildwall_skins() {
	return Buildwall_Skins_Setup::get_instance();
}

buildwall_skins();
