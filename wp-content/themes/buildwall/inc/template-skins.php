<?php
/**
 * Skins Template Functions.
 *
 * @package Buildwall
 */

/**
 * Load a template part into a template
 *
 * @since 1.0.0
 *
 * @param string $slug The slug name for the generic template.
 * @param string $name The name of the specialised template.
 */
function buildwall_get_template_part( $slug, $name = null ) {

	if ( ! function_exists( 'buildwall_skins' ) ) {
		get_template_part( $slug, $name );
		return;
	}

	$skin_path = trailingslashit( buildwall_skins()->get_current_skin_path() );

	$templates = array();
	$name = (string) $name;

	// Skins templates
	$skin_slug = $skin_path . $slug;

	if ( '' !== $name ) {
		$templates[] = "{$skin_slug}-{$name}.php";
	}

	$templates[] = "{$skin_slug}.php";

	if ( '' !== $name ) {
		$templates[] = "{$slug}-{$name}.php";
	}

	$templates[] = "{$slug}.php";

	// Allow template parts to be filtered
	$templates = apply_filters( 'buildwall_get_template_part', $templates, $slug, $name );

	locate_template( $templates, true, false );
}

/**
 * Retrieve the name of the highest priority template file that exists.
 *
 * @since 1.0.0
 *
 * @param string $template_name Template file to search for, in order.
 *
 * @return string The template filename if one is located.
 */
function buildwall_get_locate_template( $template_name ) {

	if ( ! function_exists( 'buildwall_skins' ) ) {
		return locate_template( $template_name, false, false );
	}

	$skin_path = trailingslashit( buildwall_skins()->get_current_skin_path() );

	$template_names   = array();
	$template_names[] = $skin_path . $template_name;
	$template_names[] = $template_name;

	return locate_template( $template_names, false, false );
}
