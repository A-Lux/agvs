<?php
/**
 * Template part to display a single post while in a layout posts loop
 *
 * @package Buildwall
 * @subpackage widgets
 */

?>
<div class="widget-fpblock__item invert widget-fpblock__item-<?php echo esc_attr($key); ?> widget-fpblock__item-<?php echo esc_attr( $special_class ); ?> post-<?php the_ID(); ?>">
	<div class="widget-fpblock__item-inner">
		<header class="entry-header">
			<div class="entry-meta">
				<?php echo wp_kses_post($date); ?>
				<?php echo wp_kses_post($author); ?>
				<?php echo wp_kses_post($cats); ?>
				<?php echo wp_kses_post($tags); ?>
			</div>

			<?php echo wp_kses_post($title); ?>
		</header>

		<?php echo wp_kses_post($content); ?>
	</div>
</div>
