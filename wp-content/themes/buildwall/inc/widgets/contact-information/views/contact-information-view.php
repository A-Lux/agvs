<?php
/**
 * Template part to display Contact Information widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>

<li class="contact-information__item <?php echo esc_attr($item_mod_class); ?>">
	<?php echo wp_kses_post($icon); ?>
	<?php echo wp_kses_post($text); ?>
</li>
