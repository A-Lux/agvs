<?php
/**
 * Represents the view for the `Cherry Trending Posts` widget.
 *
 * @package   Cherry_Trending_Posts
 * @author    Template Monster
 * @license   GPL-3.0+
 * @copyright 2012 - 2016, Template Monster
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
} ?>

<div class="cherry-trend-widget-list__item cherry-trend-post">
	<?php echo wp_kses_post($image); ?>
	<div class="cherry-trend-post__header">
		<?php echo '<h4 class="cherry-trend-post__title">' . $title . '</h4>'; ?>
	</div>
	<div class="cherry-trend-post__content">
		<?php echo wp_kses_post($excerpt); ?>

		<div class="cherry-trend-post__meta cherry-trend-post__meta-first entry-meta"><?php
			echo wp_kses_post($date);
			echo wp_kses_post($author);
			echo wp_kses_post($category);
		?></div>
		<div class="cherry-trend-post__meta cherry-trend-post__meta-second entry-meta"><?php
			echo wp_kses_post($tag);
			echo wp_kses_post($comments);
			echo wp_kses_post($view);
			echo wp_kses_post($rating);
		?></div>

	</div>
	<?php echo wp_kses_post($button); ?>
</div>
