<?php
/**
 * Template part to display Carousel widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>

<div class="inner">
	<div class="content-wrapper">
		<figure class="post-thumbnail">
			<?php echo wp_kses_post($image); ?>
		</figure>
		<div class="entry-meta">
			<?php echo wp_kses_post($date); ?>
			<?php echo wp_kses_post($author); ?>
			<?php echo wp_kses_post($terms_line); ?>
		</div>
		<header class="entry-header">
			<?php echo wp_kses_post($title); ?>
		</header>
		<div class="entry-content">
			<?php echo wp_kses_post($content); ?>
		</div>
		<footer class="entry-footer">
			<?php echo wp_kses_post($more_button); ?>
		</footer>
	</div>
</div>
