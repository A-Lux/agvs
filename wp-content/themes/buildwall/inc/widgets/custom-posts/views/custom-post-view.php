<?php
/**
 * Template part to display Custom posts widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>
<div class="custom-posts__item post <?php echo esc_attr( $grid_class ); ?>">
	<div class="post-inner">
		<div class="post-thumbnail">
			<?php echo wp_kses_post($category); ?>
			<?php echo wp_kses_post($image); ?>
		</div>
		<div class="post-content-wrap">
			<div class="entry-header">
				<?php echo wp_kses_post($title); ?>
			</div>
			<div class="entry-meta header-meta">
				<?php echo wp_kses_post($date); ?>
				<?php echo wp_kses_post($author); ?>
			</div>
			<div class="entry-content">
				<?php echo wp_kses_post($excerpt); ?>
			</div>
			<div class="entry-footer">
				<div class="entry-meta"><?php
					echo wp_kses_post($tag);
					echo wp_kses_post($count);
				?></div>
				<?php echo wp_kses_post($button); ?>
			</div>
		</div>
	</div>
</div>
