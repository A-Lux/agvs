<?php
/**
 * Template part to display About Buildwall widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>

<div class="widget-about__logo">
	<a class="widget-about__logo-link" href="<?php echo esc_url($home_url); ?>">
		<img class="widget-about__logo-img" src="<?php echo esc_url($logo_url); ?>" alt="<?php echo esc_attr($site_name); ?>">
	</a>
</div>
<div class="widget-about__tagline"><?php echo wp_kses_post($tagline); ?></div>
<div class="widget-about__content"><?php echo wp_kses_post($content); ?></div>
<?php echo wp_kses_post($this->get_social_nav( '<div class="widget-about__social">%s</div>' )); ?>
