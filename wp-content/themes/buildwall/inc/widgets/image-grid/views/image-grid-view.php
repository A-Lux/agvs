<?php
/**
 * Template part to display Image grid widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>
<div class="widget-image-grid__holder invert <?php echo esc_attr($columns_class); ?>">
	<figure class="widget-image-grid__inner">
		<?php echo wp_kses_post($image); ?>
		<figcaption class="widget-image-grid__content">
			<?php echo wp_kses_post($title); ?>

			<div class="entry-meta"><?php
				echo wp_kses_post($date);
				echo wp_kses_post($author);
				echo wp_kses_post($terms);
			?></div>
		</figcaption>
	</figure>
</div>
