<?php
/**
 * Template part to display full-view news-smart-box widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */

?>
<div class="news-smart-box__item-inner">
	<div class="news-smart-box__item-header">
		<?php echo wp_kses_post($image); ?>
	</div>
	<div class="news-smart-box__item-content">
		<div class="entry-meta"><?php
			echo wp_kses_post( $date );
			echo wp_kses_post( $author );
			echo wp_kses_post( $category );
			echo wp_kses_post( $tags );
			echo wp_kses_post( $comments );
		?></div>

		<?php echo wp_kses_post( $title ); ?>
		<?php echo wp_kses_post( $excerpt ); ?>
		<?php echo wp_kses_post( $more_btn ); ?>
	</div>
</div>
