<?php
/**
 * Template part to display loop-view news-smart-box widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>
<div id="news-smart-box-<?php echo esc_attr( $instance ); ?>" <?php esc_attr($data_attr_line); ?>>
	<?php echo wp_kses( $this->get_navigation_box( $current_term_slug, $alt_terms_slug_list ),
        array(
            'div' => array(
                'class' => true,
                'data-slug' => true,
            ),
        )); ?>
	<div class="news-smart-box__wrapper">
		<div class="news-smart-box__listing row">
			<?php echo wp_kses_post($this->get_instance( $current_term_slug )); ?>
		</div>
	</div>
</div>
