<?php
/**
 * Template part to display thumbnails Playlist-slider widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>
<div class="sp-thumbnail post-<?php the_ID(); ?>">
	<div class="sp-thumbnail-image-container">
		<?php echo wp_kses_post($image); ?>
	</div>
	<div class="sp-thumbnail-text">
		<div class="entry-meta"><?php
			echo wp_kses_post($date);
			echo wp_kses_post($author);
			echo wp_kses_post($comments);
		?></div>
		<?php echo wp_kses_post($title); ?>
	</div>
</div>
