<?php
/**
 * Template part to display slides Playlist-slider widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>
<div class="sp-slide format-<?php echo esc_attr( $post_format ); ?> <?php echo esc_attr( $is_invert ); ?> sp-slide--<?php echo esc_attr( $visible_content ); ?> post-<?php the_ID(); ?>">
	<div class="sp-layer" data-position="bottomLeft" data-horizontal="0" data-show-transition="up" data-show-delay="500" data-hide-transition="down">
		<div class="entry-meta"><?php
			echo wp_kses_post($date);
			echo wp_kses_post($author);
			echo wp_kses_post($category);
			echo wp_kses_post($tag);
			echo wp_kses_post($comments);
		?></div>
		<header class="entry-header">
			<?php echo wp_kses_post($title); ?>
		</header>
	</div>
	<?php echo wp_kses_post($slide); ?>
</div>
