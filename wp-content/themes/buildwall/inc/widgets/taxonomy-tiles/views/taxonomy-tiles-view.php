<?php
/**
 * Template part to display Taxonomy-tiles widget.
 *
 * @package Buildwall
 * @subpackage widgets
 */
?>
<div class="widget-taxonomy-tiles__holder invert grid-item <?php echo wp_kses_post($class); ?> term-<?php echo wp_kses_post($term->term_id); ?>">
	<figure class="widget-taxonomy-tiles__inner">
		<a href="<?php echo esc_url($permalink); ?>"><?php echo wp_kses_post($image); ?></a>
		<figcaption class="widget-taxonomy-tiles__content">
			<div class="widget-taxonomy-tiles__row">
				<?php echo wp_kses_post($title); ?>
				<?php echo wp_kses_post($count); ?>
			</div>

			<div class="widget-taxonomy-tiles__row widget-taxonomy-tiles__hidden-content">
				<?php echo wp_kses_post($description); ?>

				<a href="<?php echo  esc_url($permalink); ?>" class="widget-taxonomy-tiles__permalink"><i class="nc-icon-mini arrows-1_simple-right"></i></a>
			</div>
		</figcaption>
	</figure>
</div>
