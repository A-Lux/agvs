<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // disable direct access
}

add_action( 'wp_enqueue_scripts', 'jet_menu_phlox_pro_styles', 0 );

/**
 * Enqueue Phlox Pro compatibility styles
 *
 * @return void
 */
function jet_menu_phlox_pro_styles() {
	wp_enqueue_style(
		'jet-menu-phlox-pro',
		jet_menu()->get_theme_url( 'assets/css/style.css' ),
		array(),
		jet_menu()->get_version()
	);
}
