<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // disable direct access
}

add_action( 'wp_enqueue_scripts', 'jet_menu_phlox_styles', 0 );

/**
 * Enqueue Phlox compatibility styles
 *
 * @return void
 */
function jet_menu_phlox_styles() {
	wp_enqueue_style(
		'jet-menu-phlox',
		jet_menu()->get_theme_url( 'assets/css/style.css' ),
		array(),
		jet_menu()->get_version()
	);
}
